# 20190529-SureshKumarKumaravel-NYCSchools

1. Developed using Java (main) and Kotlin.
2. Used Android architectural components such as ViewModel, LiveData, DataBinding by following MVVM.
3. This app uses the open API https://data.cityofnewyork.us/resource/ to get the list of NYC High Schools and its SAT score requirements.
4. Used Kotlin Coroutines for performing background tasks and Spring Rest for Android to make network calls.


App flow:

1. Once the app is opened it displays the list of NYC High schools.
2. Selecting a school will open up a new screen to display additional details.
3. API error cases are handled in a visible way.
4. API calls are made only once and is cached locally.
5. With the use of ViewModel, the application is safe with the configuration change. Since ViewModels are designed to hold and manage UI-related data in a life-cycle conscious way and allows data to survive configuration changes such as screen rotations.


Test Coverage:

1. Few JUnit test cases for ViewModel including for LiveData events.
2. Android Instrumentation test case for a simple happy path.